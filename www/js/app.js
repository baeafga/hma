// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'chart.js'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Each tab has its own nav history stack:

  .state('tab.dash', {
    url: '/dash',
    views: {
      'tab-dash': {
        templateUrl: 'templates/tab-dash.html',
        controller: 'DashCtrl'
      }
    }
  })

  .state('tab.chats', {
      url: '/chats',
      views: {
        'tab-chats': {
          templateUrl: 'templates/tab-chats.html',
          controller: 'ChatsCtrl'
        }
      }
    })
    .state('tab.chat-detail', {
      url: '/chats/:chatId',
      views: {
        'tab-chats': {
          templateUrl: 'templates/chat-detail.html',
          controller: 'ChatDetailCtrl'
        }
      }
    })

  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/dash');

})

.controller("AccessController", function($scope,  $http, $interval) {
  $scope.canAccess = 1;
  $scope.check = function(){
    console.log($scope.key);
    if($scope.key == "helou"){
      $scope.canAccess = 1;
    }
    else{
      $scope.canAccess = 0; 
    }
    console.log($scope.canAccess);
  }
})

.controller("ExampleController", function($scope,  $http, $interval) {

    $scope.labels = ["1", "2", "3", "4", "5", "6", "7"];
    $scope.series = ['Data'];
    $scope.wind = [
        [0, 0, 0, 0, 0, 0, 0]
    ];
    $scope.angle = [
        [0, 0, 0, 0, 0, 0, 0]
    ];
    $scope.currentDrag = 0;
    $scope.currentLift = 0;
    $scope.updateData = function () {
      $scope.currentSpeed = $scope.wind[0][6].toFixed(2);
      $scope.currentAngle = $scope.angle[0][6].toFixed(2);
      var i;
      for(i=1; i<$scope.wind[0].length; i++){
        $scope.wind[0][i-1] = $scope.wind[0][i];
        $scope.angle[0][i-1] = $scope.angle[0][i];
      }
      $http.get("https://ancient-beach-55720.herokuapp.com/tests.json").then(function(resp){
      //console.log('Success', resp); // JSON object
      $scope.wind[0][i-1] = resp.data[0].velocity - 0;
      $scope.angle[0][i-1] = resp.data[0].angle - 0;
      $scope.currentDrag = (resp.data[0].drag - 0).toFixed(2);;
      $scope.currentLift = (resp.data[0].lift - 0).toFixed(2);;
    }, function(err){
      //console.error('ERR', err);
    })
    };
    $interval($scope.updateData, 200);
    $scope.colors = [ '#5C6BC0', '#42A5F5', '#7E57C2', '#81D4FA', '#80DEEA', '#80CBC4', '#004D40'];
})

.controller("ControlController", function($scope,  $http, $interval) {
    $scope.intendedVelocity = 0;
    var currentIntendedVelocity = 0;
    var step = 1;
    // see examples/bubble.js for random bubbles source code
    $http.get("https://ancient-beach-55720.herokuapp.com/tests.json").then(function(resp){
        //console.log('Success', resp); // JSON object
        $scope.intendedVelocity = (resp.data[0].velocity - 0).toFixed(2);
        currentIntendedVelocity = $scope.intendedVelocity ;
      }, function(err){
        //console.error('ERR', err);
      });
    $scope.reset = function(){
        var req = {
          method: 'PUT', 
          url: "https://ancient-beach-55720.herokuapp.com/tests/1",
          data: JSON.stringify({reset: true }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded',
          'Content-Type': 'application/json'}
      }
      $http(req).success(function(data) {
        console.log(data.reset);
      }).error(function(data) { 
          //console.log(data); 
      });
    }
    $scope.intendedVelocityDown = function(){
      currentIntendedVelocity -= step;
      if(currentIntendedVelocity < 0) currentIntendedVelocity = 0;
      $scope.intendedVelocity = currentIntendedVelocity;
    };
    $scope.intendedVelocityUp = function(){
      currentIntendedVelocity += step;
      if(currentIntendedVelocity >= 12) currentIntendedVelocity = 12;
      $scope.intendedVelocity = currentIntendedVelocity;
    };
    $scope.getVelocity = function(){
      return currentIntendedVelocity;
    }
    $scope.labels = ["1", "2", "3", "4", "5", "6", "7"];
    $scope.series = ['Series A'];
    $scope.data = [
        [0, 0, 0, 0, 0, 0, 0]
    ];
    $scope.updateData = function () {
      var i;
      for(i=1; i<$scope.data[0].length; i++){
        $scope.data[0][i-1] = $scope.data[0][i];
      }
      $scope.currentSpeed = $scope.data[0][6].toFixed(2);
      $http.get("https://ancient-beach-55720.herokuapp.com/tests.json").then(function(resp){
        //console.log('Success', resp); // JSON object
        $scope.data[0][i-1] = resp.data[0].velocity - 0;
      }, function(err){
        //console.error('ERR', err);
      });
      var req = {
          method: 'PUT', 
          url: "https://ancient-beach-55720.herokuapp.com/tests/1",
          data: JSON.stringify({intended_velocity: $scope.intendedVelocity, velocity : Math.random()*12 + 1, drag : Math.random()*12 + 1, lift : Math.random()*12 + 1, angle : Math.random()*30 + 1 }),
          headers: {'Content-Type': 'application/x-www-form-urlencoded',
          'Content-Type': 'application/json'}
      }
      $http(req).success(function(data) {
        //console.log(data);
      }).error(function(data) { 
          //console.log(data); 
      });
    };
    $interval($scope.updateData, 200);
});
